# Graphviz Windows Dependencies

These precompiled libraries are used for building and running Graphviz on Windows. This repository contains ('yes' mean present, but unkown version):

- [ANN: A Library for Approximate Nearest Neighbor Searching](http://www.cs.umd.edu/~mount/ANN/) (version 1.12)
- atk (version 1.22)
- Cairo
- [The Expat XML Parser](http://www.libexpat.org/) (version 2.2.0)
- Fontconfig (only .lib and .dll)
- [FreeGLUT](http://www.transmissionzero.co.uk/software/freeglut-devel/) (version 3.0.0)
- Freetype (only .lib and .dll)
- [GD Graphics library](https://libgd.github.io/) (version 2.2.0)
- glib (version 2.16.3)
- Iconv (only .lib and .dll)
- jpeg (only .lib and .dll)
- GTK (version 2.12.9)
- [LibTool](http://gnuwin32.sourceforge.net/packages/libtool.htm) (version 1.5.26)
- Pango and pangocairco
- PNG
- zlib (version 1.2.3)
